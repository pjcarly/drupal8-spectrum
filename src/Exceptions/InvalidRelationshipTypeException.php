<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidRelationshipTypeException extends Exception
{ }
