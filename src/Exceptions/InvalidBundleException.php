<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidBundleException extends Exception
{ }
