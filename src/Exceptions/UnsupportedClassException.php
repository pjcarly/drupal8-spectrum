<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class UnsupportedClassException extends Exception
{ }
