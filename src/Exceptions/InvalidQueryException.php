<?php

namespace Drupal\spectrum\Exceptions;

use Exception;

class InvalidQueryException extends Exception
{ }
