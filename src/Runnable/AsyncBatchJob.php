<?php

declare(strict_types=1);

namespace Drupal\spectrum\Runnable;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\spectrum\Runnable\State\StateInterface;
use Error;
use Exception;
use RuntimeException;
use Throwable;

abstract class AsyncBatchJob extends BatchJob
{

  private readonly StateInterface $state;

  public function __construct(EntityInterface $entity)
  {
    parent::__construct($entity);
    $this->state = Drupal::service('spectrum.runnable.state');
  }

  public static function callOnNext(int $id, int $chunkSize): void
  {
    static::forgeById($id)?->onNext($chunkSize);
  }

  public static function callOnError(int $id, Throwable $t): void
  {
    static::forgeById($id)?->onError($t);
  }

  public function onError(Throwable $t): void
  {
    $this->state->delete("spectrum.runnable.{$this->getId()}.records_processed");
    $this->state->delete("spectrum.runnable.{$this->getId()}.total_records");

    match (true) {
      $t instanceof Error => $this->runtimeError($t),
      $t instanceof Exception => $this->failedExecution(null, $t->getMessage()),
    };
  }

  public function onNext(int $chunkSize): void
  {
    $this->accountSwitcher ??= Drupal::service('account_switcher');

    if ($this->getRunAsUserId() === NULL || $this->getRunAsUserId() === 0 || $this->fetch('run_as') === NULL) {
      $account = new AnonymousUserSession;
    } else {
      $account = $this->getRunAsUser()->entity;
    }

    $this->accountSwitcher->switchTo($account);

    $amountOfRecordsProcessed = $this->state->increaseBy(
      "spectrum.runnable.{$this->getId()}.records_processed",
      $chunkSize
    );

    $totalAmountOfRecords = (int) $this->state->get("spectrum.runnable.{$this->getId()}.total_records");
    $this->updateCronStatus($amountOfRecordsProcessed, $totalAmountOfRecords);

    $this->clearCache();

    if ($amountOfRecordsProcessed >= $totalAmountOfRecords) {
      $this->state->delete("spectrum.runnable.{$this->getId()}.records_processed");
      $this->state->delete("spectrum.runnable.{$this->getId()}.total_records");
      $this->afterExecute();
      $this->postExecution();
    }
  }

  public function print(string $message): void
  {
  }

  public function execute(): void
  {
    $batchable = $this->getBatchable();
    $batchable->setBatchSize($this->getBatchSize());

    $totalBatchedRecords = $batchable->getTotalBatchedRecords();

    if ($totalBatchedRecords === 0) {
      $this->setStatus(self::STATUS_COMPLETED);
      $this->save();

      $this->updateCronStatus(0, 0);
      return;
    }

    $this->state->set(
      "spectrum.runnable.{$this->getId()}.total_records",
      $totalBatchedRecords
    );

    $this->state->set(
      "spectrum.runnable.{$this->getId()}.records_processed",
      0
    );

    $this->updateCronStatus(0, $batchable->getTotalBatchedRecords());

    foreach ($batchable->getBatchChunkedGenerator() as $chunk) {
      $this->processChunk($chunk);
    }
  }

  public function run(): void
  {
    try {
      $this->fetch('job');

      $this->preExecution();
      $this->execute();
      $this->accountSwitcher->switchBack();
    } catch (Exception $e) {
      $this->failedExecution($e);
    } catch (Error $e) {
      $this->runtimeError($e);
    }
  }

  final protected function process(EntityInterface $entity): void
  {
    throw new RuntimeException(__CLASS__ . ' only supports chunked processing');
  }
}
