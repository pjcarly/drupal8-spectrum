<?php

namespace Drupal\spectrum\Runnable;

use DateTime;
use DateTimeZone;
use Drupal;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\spectrum\Event\CronStatusUpdatedEvent;
use Drupal\spectrum\Services\ModelStoreInterface;
use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

abstract class BatchJob extends QueuedJob
{
  protected ModelStoreInterface $modelStore;
  protected MemoryCacheInterface $memoryCache;
  protected EventDispatcherInterface $eventDispatcher;
  protected bool $chunked = false;

  public function __construct(EntityInterface $entity)
  {
    parent::__construct($entity);
    $this->modelStore = Drupal::service('spectrum.model_store');
    $this->memoryCache = Drupal::service('entity.memory_cache');
    $this->eventDispatcher = Drupal::service('event_dispatcher');
  }

  /**
   * {@inheritdoc}
   */
  public static function scheduleBatch(
    string $jobName,
    string $variable = null,
    DateTime $date = null,
    int $batchSize = null,
    string $relatedEntity = null,
    string $relatedBundle = null,
    string|int $relatedModelId = null
  ): BatchJob {
    $registeredJob = RegisteredJob::getByKey($jobName);

    if ($registeredJob === NULL) {
      throw new Exception('Registered Job (' . $jobName . ') not found');
    }

    if ($date === NULL) {
      $date = new DateTime('now', new DateTimeZone('UTC'));
    }

    /** @var BatchJob $queuedJob */
    $queuedJob = $registeredJob->createJobInstance();
    $queuedJob->setTitle($jobName);

    if (!empty($variable)) {
      $queuedJob->setVariable($variable);
    }

    $queuedJob->setBatchSize($batchSize);
    $queuedJob->setMinutesToFailure(10);
    $queuedJob->setScheduledTime($date);
    $queuedJob->setRelatedEntity($relatedEntity);
    $queuedJob->setRelatedBundle($relatedBundle);
    $queuedJob->setRelatedModelId($relatedModelId);

    $queuedJob->put('job', $registeredJob);
    $queuedJob->save();

    return $queuedJob;
  }

  public function execute(): void
  {
    $batchable = $this->getBatchable();
    $batchSize = $this->getBatchSize();
    $batchable->setBatchSize($batchSize);
    $totalRecords = $batchable->getTotalBatchedRecords();

    $progressBar = new ProgressBar($this->output, $totalRecords);
    $progressBar->setFormat('debug');
    $progressBar->start();
    $totalCounter = 0;

    $this->updateCronStatus($totalCounter, $totalRecords);

    if ($this->chunked) {
      foreach ($batchable->getBatchChunkedGenerator() as $chunk) {
        $this->processChunk($chunk);

        $chunkSize = count($chunk);
        $progressBar->advance($chunkSize);
        $totalCounter += $chunkSize;

        $this->clearCache();

        $this->updateCronStatus($totalCounter, $totalRecords);
      }
    } else {
      foreach ($batchable->getBatchGenerator() as $entity) {
        $this->process($entity);

        $progressBar->advance();

        $totalCounter++;

        if ($totalCounter % $batchSize === 0) {
          $this->clearCache();
          $this->updateCronStatus($totalCounter, $totalRecords);
        }
      }
    }

    $progressBar->finish();
    $this->getOutput()->writeln('');

    $this->afterExecute();
  }

  /**
   * Hook that is called after the batch job is fully executed
   *
   * @return void
   */
  protected function afterExecute(): void
  {
  }

  protected function getSleep(): float
  {
    $sleep = $this->entity->{'field_sleep'}->value;
    return empty($sleep) || ($sleep <= 0) ? 0 : $sleep;
  }

  public function getBatchSize(): int
  {
    return $this->entity->{'field_batch_size'}->value ?? 200;
  }

  public function setBatchSize(?int $value): QueuedJob
  {
    $this->entity->{'field_batch_size'}->value = $value;
    return $this;
  }

  protected abstract function process(EntityInterface $entity): void;
  protected abstract function getBatchable(): BatchableInterface;

  /**
   * Processes a chunk of entities
   *
   * @param EntityInterface[] $chunk
   *
   * @return void
   */
  protected function processChunk(array $chunk): void
  {
  }

  protected function clearCache()
  {
    // This will clear all the entity caches, and free entities from memory
    Drupal::service('spectrum.model')->clearDrupalEntityCachesForAllModels();

    $this->memoryCache->deleteAll();

    // And finally clear the model store of any data as well
    $this->modelStore->unloadAll();
  }

  /**
   * @param int $totalCounter
   * @param int|null $totalRecords
   *
   * @return void
   */
  protected function updateCronStatus(int $totalCounter, ?int $totalRecords): void
  {
    if ($this->updateCronStatus) {
      $event = new CronStatusUpdatedEvent($this, $totalCounter, $totalRecords);
      $this->eventDispatcher->dispatch($event);
    }
  }
}
