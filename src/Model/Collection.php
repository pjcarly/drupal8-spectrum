<?php

namespace Drupal\spectrum\Model;

use ArrayIterator;
use Countable;
use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\spectrum\Exceptions\InvalidTypeException;
use Drupal\spectrum\Query\BundleQuery;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Query\Query;
use Drupal\spectrum\Services\ModelServiceInterface;
use Generator;
use IteratorAggregate;

/**
 * A collection holds multiple models. It also tracks models that were removed
 * from the collection between the initialization and save. So deletes can be
 * done for models that were removed. It can be used to fetch, validate and
 * save related records defined by Relationships on Models
 *
 * A collection respects the UnitOfWork design pattern. Together with Model and
 * Relationship, this is the Core of the Spectrum framework This functionality
 * is loosely based on BookshelfJS (http://bookshelfjs.org/)
 *
 * @template T
 */
class Collection implements IteratorAggregate, Countable, CollectionInterface {

  /**
   * The FQCN of the models in this Collection
   *
   * @var ?class-string<T>
   */
  protected ?string $modelType;

  /**
   * Array containing all the Models of the Collection. The array is keyed by
   * the "key" property of the model. In case the model exists in the DB this
   * is the ID, if not this is a placeholder key
   *
   * @var ModelInterface[]
   */
  protected array $models = [];

  /**
   * Array containing all the original Models of the Collection. The array is
   * keyed by the "key" property of the model. In case the model exists in the
   * DB this is the ID, if not this is a placeholder key When saving a
   * collection, all the models that exist in the `originalModels` array, but
   * do not exist in the `models` array are removed
   *
   * @var ModelInterface[]
   */
  protected array $originalModels = [];

  /**
   * T[]
   */
  public function getModels(): array {
    return $this->models;
  }

  /**
   * T[]
   */
  public function getOriginalModels(): array {
    return $this->originalModels;
  }

  public function count(): int {
    return sizeof($this->models);
  }

  public function getIterator(): ArrayIterator {
    return new ArrayIterator($this->models);
  }

  /**
   * {@inheritdoc}
   */
  public function replaceOldModelKey($oldKey, $newKey): self {
    if (array_key_exists($oldKey, $this->models)) {
      $model = $this->models[$oldKey];
      unset($this->models[$oldKey]);
      $this->models[$newKey] = $model;
    }

    if (array_key_exists($oldKey, $this->originalModels)) {
      $originalModel = $this->originalModels[$oldKey];
      unset($this->originalModels[$oldKey]);
      $this->originalModels[$newKey] = $originalModel;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save(string $relationshipName = NULL): self {
    if (empty($relationshipName)) {
      foreach ($this->models as $model) {
        $model->save();
      }

      foreach ($this->getModelsToDelete() as $modelToDelete) {
        $modelToDelete->delete();
      }
    } else {
      $this->get($relationshipName)->save();
    }

    return $this;
  }

  /**
   * @return ?T
   */
  public function first(): ?ModelInterface {
    if ($element = reset($this->models)) {
      return $element;
    }

    return NULL;
  }

  /**
   * @return ?T
   */
  public function last(): ?ModelInterface {
    if ($element = end($this->models)) {
      return $element;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getModelOnPosition(int $number): ?ModelInterface {
    $sliced = array_slice($this->models, $number, 1);
    return sizeof($sliced) > 0 ? $sliced[0] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTranslation(array $languageCodes): self {
    foreach ($this->models as $model) {
      $model->loadTranslation($languageCodes);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function sort($sortingFunction): self {
    if (is_string($sortingFunction)) {
      // Bug in PHP causes PHP warnings for uasort, we suppressed warnings with @, but be weary!
      @uasort($this->models, [$this->modelType, $sortingFunction]);
    } elseif (is_callable($sortingFunction)) {
      uasort($this->models, $sortingFunction);
    }

    return $this;
  }

  /**
   * T[]
   */
  public function getModelsToDelete(): array {
    $existingRemovedModels = [];
    $removedModelKeys = array_diff(array_keys($this->originalModels), array_keys($this->models));

    foreach ($removedModelKeys as $removedModelKey) {
      $removedModel = $this->originalModels[$removedModelKey];
      if (!$removedModel->isNew()) {
        $existingRemovedModels[$removedModel->getModelIdentifier()] = $removedModel;
      }
    }

    return $existingRemovedModels;
  }

  /**
   * Remove a model by key from the collection
   *
   * @param string|int $key
   *
   * @return self
   */
  private function remove($key): self {
    if (array_key_exists($key, $this->models)) {
      unset($this->models[$key]);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeModel(ModelInterface $model): self {
    return $this->remove($model->getModelIdentifier());
  }

  /**
   * {@inheritdoc}
   */
  public function removeAll(): self {
    $this->models = [];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeNonSelectedModels(): self {
    foreach ($this->models as $model) {
      if (!$model->selected) {
        $this->removeModel($model);
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeSelectedModels(): self {
    foreach ($this->models as $model) {
      if ($model->selected) {
        $this->removeModel($model);
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountOfSelectedModels(): int {
    $count = 0;
    foreach ($this->models as $model) {
      if ($model->selected) {
        $count++;
      }
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountOfNotSelectedModels(): int {
    $count = 0;
    foreach ($this->models as $model) {
      if (!$model->selected) {
        $count++;
      }
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountOfNewModels(): int {
    $count = 0;
    foreach ($this->models as $model) {
      if ($model->isNew()) {
        $count++;
      }
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function selectAll(): self {
    foreach ($this->models as $model) {
      $model->selected = TRUE;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function deselectAll(): self {
    foreach ($this->models as $model) {
      $model->selected = FALSE;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(string $relationshipName = NULL): Validation {
    if (empty($relationshipName)) {
      $validation = NULL;
      foreach ($this->models as $model) {
        if (empty($validation)) {
          $validation = $model->validate();
        } else {
          $validation->merge($model->validate());
        }
      }

      return $validation;
    }

    return $this->get($relationshipName)->validate();
  }

  /**
   * {@inheritdoc}
   */
  public function clear(string $relationshipName): self {
    foreach ($this->models as $model) {
      $model->clear($relationshipName);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(string $relationshipName, ?Query $queryToCopyFrom = NULL): CollectionInterface {
    $returnValue = NULL;
    $lastRelationshipNameIndex = strrpos($relationshipName, '.');

    if (empty($lastRelationshipNameIndex)) // relationship name without extra relationships
    {
      $modelType = $this->getModelType();
      /** @var Relationship $relationship */
      $relationship = $modelType::getRelationship($relationshipName);
      $relationshipQuery = $relationship->getRelationshipQuery();

      // Lets see if we need to copy in some default conditions
      if (!empty($queryToCopyFrom)) {
        $relationshipQuery->copyConditionsFrom($queryToCopyFrom);
        $relationshipQuery->copySortOrdersFrom($queryToCopyFrom);
        $relationshipQuery->setUserIdForAccessPolicy($queryToCopyFrom->getUserIdForAccessPolicy());
        $relationshipQuery->setAccessPolicy($queryToCopyFrom->getAccessPolicy());

        if ($queryToCopyFrom->hasLimit()) {
          $relationshipQuery->setRange($queryToCopyFrom->getRangeStart(), $queryToCopyFrom->getRangeLength());
        }
      }

      $relationshipCondition = $relationship->getCondition();

      if ($relationship instanceof FieldRelationship) {
        $fieldIds = $this->getFieldIds($relationship);
        if (!empty($fieldIds)) {
          // we set the field ids in the condition, and fetch a collection of models with that id in a field
          $relationshipCondition->setValue($fieldIds);
          $relationshipQuery->addCondition($relationshipCondition);
          $referencedEntities = $relationshipQuery->fetch();

          if (!empty($referencedEntities)) {
            // Here we will build a collection of the entities we fetched
            $referencedModelType = NULL;
            $referencedCollection = NULL;
            foreach ($referencedEntities as $referencedEntity) {
              $referencedModel = NULL;
              if ($relationship->isPolymorphic || empty($referencedModelType)) {
                // if the relationship is polymorphic we can get multiple bundles, so we must define the modeltype based on the bundle and entity of the current looping entity
                // or if the related modeltype isn't set yet, we must set it once
                $referencedEntityType = $referencedEntity->getEntityTypeId();
                $referencedEntityBundle = NULL;
                if (isset($referencedEntity->type->target_id)) {
                  // with all entity references this shouldn't be a problem, however, in case of 'user', this is blank
                  // Luckily we handle this correctly in getModelClassForEntityAndBundle
                  $referencedEntityBundle = $referencedEntity->type->target_id;
                }
                $referencedModelType = Drupal::service('spectrum.model')
                  ->getModelClassForEntityAndBundle($referencedEntityType, $referencedEntityBundle);

                // lets also check if a collection has been made already, and if not, lets make one (keeping in mind polymorphic relationships)
                if ($referencedCollection == NULL) {
                  if ($relationship->isPolymorphic) {
                    $referencedCollection = PolymorphicCollection::forgeNew(NULL);
                  } else {
                    $referencedCollection = Collection::forgeNew($referencedModelType);
                  }
                }
              }
              // we can finally forge a new model
              $referencedModel = $referencedModelType::forgeByEntity($referencedEntity);
              // and put it in the collection created above
              $referencedCollection->putOriginal($referencedModel);
              $returnValue = $referencedCollection->put($referencedModel);
            }

            $this->putInverses($relationship, $referencedCollection);
          }
        }

        if (empty($returnValue)) {
          if ($relationship->isPolymorphic) {
            $returnValue = PolymorphicCollection::forgeNew(NULL);
          } else {
            $returnValue = Collection::forgeNew($relationship->getModelType());
          }
        }
      } else {
        if ($relationship instanceof ReferencedRelationship) {
          $modelIds = $this->getIds();

          if (!empty($modelIds)) {
            $relationshipCondition->setValue($modelIds);
            $relationshipQuery->addCondition($relationshipCondition);

            $referencingEntities = $relationshipQuery->fetch();

            if (!empty($referencingEntities)) {
              $referencingCollection = NULL;
              $referencingModelType = NULL;
              foreach ($referencingEntities as $referencingEntity) {
                $referencingModel = NULL;
                if (empty($referencingModelType)) {
                  // if the relationship is polymorphic we can get multiple bundles, so we must define the modeltype based on the bundle and entity of the current looping entity
                  // or if the referencing modeltype isn't set yet, we must set it once
                  $referencingEntityType = $referencingEntity->getEntityTypeId();
                  $referencingEntityBundle = $referencingEntity->type->target_id;
                  $referencingModelType = Drupal::service('spectrum.model')
                    ->getModelClassForEntityAndBundle($referencingEntityType, $referencingEntityBundle);

                  if ($referencingCollection === NULL) {
                    // referencedRelationships are never polymorphics
                    $referencingCollection = Collection::forgeNew($referencingModelType);
                  }
                }

                // now that we have a model, lets put them one by one
                $referencingModel = $referencingModelType::forgeByEntity($referencingEntity);
                $referencingCollection->putOriginal($referencingModel);
                $returnValue = $referencingCollection->put($referencingModel);
              }
            }

            if (!empty($referencingCollection)) {
              $this->putInverses($relationship, $referencingCollection);
            }
          }

          if (empty($returnValue)) {
            $returnValue = Collection::forgeNew($relationship->getModelType());
          }
        }
      }
    } else {
      $secondToLastRelationshipName = substr($relationshipName, 0, $lastRelationshipNameIndex);
      $resultCollection = $this->get($secondToLastRelationshipName);
      $lastRelationshipName = substr($relationshipName, $lastRelationshipNameIndex + 1);
      $returnValue = $resultCollection->fetch($lastRelationshipName, $queryToCopyFrom);
    }

    return $returnValue;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $models = $this->models;

    $ids = [];
    foreach ($models as $model) {
      $id = $model->getId();
      if (!empty($id)) {
        $ids[$id] = $id;
      }
    }

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldIds(FieldRelationship $relationship): array {
    $fieldIds = [];

    foreach ($this->models as $model) {
      $fieldId = $model->getFieldId($relationship);
      if (!empty($fieldId)) {
        if (is_array($fieldId)) {
          $fieldIds = array_merge($fieldId, $fieldIds);
        } else {
          $fieldIds[$fieldId] = $fieldId;
        }
      }
    }

    return $fieldIds;
  }

  /**
   * {@inheritdoc}
   */
  public function buildArrayByFieldName(string $fieldName): array {
    $modelType = $this->modelType;
    $fieldDefinition = Drupal::service('spectrum.model')
      ->getFieldDefinition($modelType, $fieldName);

    switch ($fieldDefinition->getType()) {
      case 'address':
      case 'geolocation':
        throw new InvalidTypeException('You cant build an array by this field type');
      case 'entity_reference':
      case 'entity_reference_revisions':
      case 'file':
      case 'image':
        $column = 'target_id';
        break;
      case 'link':
        $column = 'uri';
        break;
      default:
        $column = 'value';
        break;
    }

    $array = [];
    foreach ($this->models as $model) {
      $key = $model->entity->$fieldName->$column;
      $array[$key] = $model;
    }

    return $array;
  }

  /**
   * Forge a new empty Collection
   *
   * @param string|null $modelType
   *
   * @return CollectionInterface
   */
  public static function forgeNew(?string $modelType): CollectionInterface {
    return static::forge($modelType, [], [], []);
  }

  /**
   * Forge a new Collection with the ids provided
   *
   * @param string|null $modelType
   * @param array $ids
   *
   * @return CollectionInterface
   */
  public static function forgeByIds(?string $modelType, array $ids): CollectionInterface {
    return static::forge($modelType, [], [], $ids);
  }

  /**
   * Forge a new Collection with the provided models
   *
   * @param string|null $modelType
   * @param ModelInterface[] $models
   *
   * @return CollectionInterface
   */
  public static function forgeByModels(?string $modelType, array $models): CollectionInterface {
    return static::forge($modelType, $models, [], []);
  }

  /**
   * Forge a new Collection with the provided entities, all entities will be
   * wrapped in a Model
   *
   * @param string|null $modelType
   * @param EntityInterface[] $entities
   *
   * @return CollectionInterface
   */
  public static function forgeByEntities(?string $modelType, array $entities): CollectionInterface {
    return static::forge($modelType, [], $entities, []);
  }

  /**
   * Forge a new Collection, try to use the more readable helper methods
   * "forgeByIds", "forgeByModels" or "forgeByEntites" instead
   *
   * @param string $modelType is optional when this is a Polymorphic collection
   * @param ModelInterface[]|null $models
   * @param EntityInterface[]|null $entities
   * @param string[]|int[]|null $ids
   *
   * @return CollectionInterface
   */
  private static function forge(string $modelType = NULL, ?array $models = [], ?array $entities = [], ?array $ids = []): CollectionInterface {
    $collection = new static();
    $collection->setModelType($modelType);

    if (is_array($ids) && !empty($ids)) {
      $entities = static::fetchEntities($modelType, $ids);
    }

    if (is_array($entities) && !empty($entities)) {
      $models = static::transformEntitiesToModels($modelType, $entities);
    }

    if (is_array($models) && !empty($models)) {
      $collection->putModels($models);
    }

    return $collection;
  }

  /**
   * @param string $modelType
   * @param string[]|int[] $ids
   *
   * @return EntityInterface[]
   */
  private static function fetchEntities(string $modelType, array $ids): array {
    /** @var ModelServiceInterface $modelService */
    $modelService = Drupal::service("spectrum.model");

    $query = new BundleQuery($modelType::entityType(), $modelType::bundle());

    $query->addCondition(new Condition($modelService->getIdField($modelType), 'IN', $ids));
    return $query->fetch();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntities(): array {
    $entities = [];
    foreach ($this->models as $model) {
      $id = $model->getId();

      $entities[$id] = $model->entity;
    }

    return $entities;
  }

  private static function transformEntitiesToModels(string $modelType, array $entities): array {
    $models = [];
    foreach ($entities as $entity) {
      $models[] = $modelType::forgeByEntity($entity);
    }
    return $models;
  }

  /**
   * Put all the provided models in the collection
   *
   * @param array $models
   *
   * @return self
   */
  private function putModels(array $models): self {
    foreach ($models as $model) {
      $this->put($model);
      $this->putOriginal($model);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function put(ModelInterface|CollectionInterface $objectToPut): self {
    if ($objectToPut instanceof CollectionInterface) {
      foreach ($objectToPut as $model) {
        $this->put($model);
      }

      // Lets loop over the original models as well, we can potentially have original models that arent in the model list.
      foreach ($objectToPut->originalModels as $originalModel) {
        $this->putOriginal($originalModel);
      }
    } else {
      if ($objectToPut instanceof ModelInterface) {
        $model = $objectToPut;
        if (!($model instanceof $this->modelType)) {
          throw new InvalidTypeException('Model is not of type: ' . $this->modelType);
        }

        $this->addModelToModels($model);
      } else {
        throw new InvalidTypeException('Only objects of type Collection or Model can be put');
      }
    }

    return $this; // we need this to chain fetches, when we put something, we always return the value where the model is being put on, in case of a collection, it is always the collection itself
  }

  /**
   * {@inheritdoc}
   */
  public function putOriginal(ModelInterface|CollectionInterface $objectToPut): self {
    if ($objectToPut instanceof CollectionInterface) {
      foreach ($objectToPut as $model) {
        $this->putOriginal($model);
      }
    } else {
      if ($objectToPut instanceof ModelInterface) {
        $model = $objectToPut;
        if (!($model instanceof $this->modelType)) {
          throw new InvalidTypeException('Model is not of type: ' . $this->modelType);
        }

        $this->addModelToOriginalModels($model);
      } else {
        throw new InvalidTypeException('Only objects of type Collection or Model can be put');
      }
    }

    return $this; // we need this to chain fetches, when we put something, we always return the value where the model is being put on, in case of a collection, it is always the collection itself
  }

  /**
   * {@inheritdoc}
   */
  public function putNew(): ModelInterface {
    $modelType = $this->modelType;
    $newModel = $modelType::forgeNew();
    $this->put($newModel);
    return $newModel;
  }

  /**
   * Add a Model to the model array
   *
   * @param ModelInterface $model
   *
   * @return self
   */
  protected function addModelToModels(ModelInterface $model): self {
    if (!array_key_exists($model->getModelIdentifier(), $this->models)) {
      $this->models[$model->getModelIdentifier()] = $model;
    }

    return $this;
  }

  /**
   * Add the provided model to the Original Models array
   *
   * @param ModelInterface $model
   *
   * @return self
   */
  protected function addModelToOriginalModels(ModelInterface $model): self {
    if (!array_key_exists($model->getModelIdentifier(), $this->originalModels)) {
      $this->originalModels[$model->getModelIdentifier()] = $model;
    }

    return $this;
  }

  /**
   * Returns the size of the collection
   *
   * @return integer
   */
  public function size(): int {
    return count($this->models);
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return empty($this->models);
  }

  /**
   * {@inheritdoc}
   */
  public function containsKey(string $key): bool {
    return array_key_exists($key, $this->models);
  }

  /**
   * {@inheritdoc}
   */
  public function containsModel(ModelInterface $model): bool {
    $key = $model->getModelIdentifier();
    return $this->containsKey($key) && $this->getModel($key) === $model;
  }

  /**
   * {@inheritdoc}
   */
  public function getModel(string $key): ?ModelInterface {
    if ($this->containsKey($key)) {
      return $this->models[$key];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $relationshipName): CollectionInterface {
    $resultCollection = NULL;
    $modelType = $this->getModelType();

    $firstRelationshipNameIndex = strpos($relationshipName, '.');

    if (empty($firstRelationshipNameIndex)) {
      $relationship = $modelType::getRelationship($relationshipName);
      $resultCollection = NULL;

      if ($relationship instanceof ReferencedRelationship) {
        $resultCollection = static::forgeNew($relationship->getModelType());
      } else {
        if ($relationship instanceof FieldRelationship) {
          if ($relationship->isPolymorphic) {
            $resultCollection = PolymorphicCollection::forgeNew(NULL);
          } else {
            $resultCollection = static::forgeNew($relationship->getModelType());
          }
        } else {
          throw new InvalidTypeException('Invalid Relationship type ');
        }
      }

      foreach ($this->models as $model) {
        $relationshipModels = $model->get($relationship);

        if (!empty($relationshipModels)) {
          $resultCollection->put($relationshipModels);
        }
      }
    } else {
      $firstRelationshipName = substr($relationshipName, 0, $firstRelationshipNameIndex);
      $newCollection = $this->get($firstRelationshipName);
      $newRelationshipName = substr($relationshipName, $firstRelationshipNameIndex + 1);

      $resultCollection = $newCollection->get($newRelationshipName);
    }

    return $resultCollection;
  }

  /**
   * Magic getter that provides helper properties on Collection
   *
   * @param string $property
   *
   * @return object
   */
  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }

    switch ($property) {
      case 'size':
        return $this->size();
      case 'isEmpty':
        return $this->isEmpty();
      case 'entities':
        return $this->getEntities();
    }
  }

  /**
   * Magic setter that prevents the overriding of the $models and
   * $originalModels properties
   *
   * @param string $property
   * @param mixed $value
   */
  public function __set($property, $value) {
    switch ($property) {
      case 'models':
      case 'originalModels':
        break;

      default:
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        break;
    }

    return $this;
  }

  /**
   * Magic isset method, for use by the Twig rendering engine
   *
   * @param string $property
   *
   * @return boolean
   */
  public function __isset($property) {
    // Needed for twig to be able to access relationship via magic getter
    return property_exists($this, $property) || in_array($property, [
        'size',
        'isEmpty',
        'entities',
      ]);
  }

  /**
   * This method sets the provided collection as the inverse for the provided
   * relationship
   *
   * @param Relationship $relationship
   * @param CollectionInterface $inverses
   *
   * @return void
   */
  private function putInverses(Relationship $relationship, CollectionInterface $inverses): void {
    $relationshipName = NULL;
    $referencingCollection = NULL;
    $referencedCollection = NULL;

    if ($relationship instanceof FieldRelationship) {
      $relationshipName = $relationship->getName();
      $referencingCollection = $this;
      $referencedCollection = $inverses;
    } else {
      /** @var ReferencedRelationship $relationship */
      $relationshipName = $relationship->fieldRelationship->getName();
      $referencingCollection = $inverses;
      $referencedCollection = $this;
    }

    /** @var Model $referencingModel */
    foreach ($referencingCollection as $referencingModel) {
      $modelFieldRelationship = $referencingModel::getRelationship($relationshipName);
      $fieldIds = $referencingModel->getFieldId($modelFieldRelationship);

      if (empty($fieldIds)) {
        $fieldIds = [];
      }

      if (!is_array($fieldIds)) {
        $fieldIds = [$fieldIds];
      }

      foreach ($fieldIds as $fieldId) {
        if ($referencedModel = $referencedCollection->getModel($fieldId)) {
          $referencingModel->put($modelFieldRelationship, $referencedModel);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refresh(): self {
    foreach ($this->models as $model) {
      $model->refresh();
    }

    return $this;
  }

  /**
   * Get the value of modelType
   */
  public function getModelType(): string {
    return $this->modelType;
  }

  /**
   * @param string|null $modelType
   *
   * @return self
   */
  public function setModelType(?string $modelType): self {
    $this->modelType = $modelType;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRelationship(string $relationshipName): bool {
    $modelType = $this->getModelType();

    return $modelType::hasRelationship($relationshipName);
  }

  public function filter(callable $function): static {
    foreach ($this as $element) {
      if (!$function($element)) {
        $this->removeModel($element);
      }
    }

    return $this;
  }

  public function yield(): Generator {
    $this->originalModels = [];

    foreach ($this->models as $model) {
      yield array_pop($this->models);
    }
  }

}
