<?php

declare(strict_types=1);

namespace Drupal\spectrum\Jobs;

use DateTime;
use DateTimeImmutable;
use Drupal;
use Drupal\spectrum\Model\ModelInterface;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Runnable\QueuedJob;
use Drupal\spectrum\Runnable\RegisteredJob;
use RuntimeException;

/**
 * @Job(
 *   id = "Drupal\spectrum\Jobs\DeleteModelJob",
 *   description = "Deletes a model.",
 * )
 */
final class DeleteModelJob extends QueuedJob {

  public function execute(): void {
    $this->fetchRelatedModel()?->delete();
  }

  public static function create(
    ModelInterface $model,
    ?DateTimeImmutable $at = NULL
  ): void {
    $registeredJob = RegisteredJob::getByKey(__CLASS__);

    if ($registeredJob === NULL) {
      throw new RuntimeException('job not found');
    }

    /** @var QueuedJob $queuedJob */
    $queuedJob = $registeredJob->createJobInstance();
    $queuedJob->put('job', $registeredJob);
    $queuedJob->setTitle(__CLASS__);
    $queuedJob->setRelatedEntity($model::entityType());
    $queuedJob->setRelatedBundle($model::bundle());
    $queuedJob->setRelatedModelId($model->getId());
    $queuedJob->setMinutesToFailure(5);

    $at ??= Drupal::service('spectrum.clock')->now();
    $queuedJob->setScheduledTime(DateTime::createFromImmutable($at));

    $queuedJob->save();
  }

  public static function deleteJobsForModel(ModelInterface $model) {
    self::getModelQuery()
      ->addCondition(new Condition(
        'field_related_entity',
        '=',
        $model::entityType(),
      ))
      ->addCondition(new Condition(
        'field_related_bundle',
        '=',
        $model::bundle()
      ))
      ->addCondition(new Condition(
        'field_related_model_id',
        '=',
        $model->getId(),
      ))
      ->addCondition(new Condition(
        'field_job_status',
        '=',
        QueuedJob::STATUS_QUEUED,
      ))
      ->fetchCollection()
      ->removeAll()
      ->save();
  }

}