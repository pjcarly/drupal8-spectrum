<?php

namespace Drupal\spectrum\Services;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\spectrum\Exceptions\ModelClassNotDefinedException;
use Drupal\spectrum\Model\FieldRelationship;
use Drupal\spectrum\Model\ModelInterface;
use Psr\Log\LoggerInterface;
use Drupal\spectrum\Models\Image;
use Drupal\spectrum\Runnable\QueuedJob;
use Drupal\spectrum\Runnable\RegisteredJob;

class ModelService implements ModelServiceInterface
{
  /**
   * This array will hold a mapping between the modelClass keys and the modelClasses
   */
  private array $modelClassMappings = [];

  public function __construct(
    private LoggerInterface $logger,
    private EntityTypeManagerInterface $entityTypeManager,
    private EntityFieldManagerInterface $entityFieldManager
  ) {
    $this->setModelClassMappings();
  }

  /**
   * Generates a mapping between the key and modelClass
   *
   * @return self
   */
  private function setModelClassMappings(): self
  {
    foreach ($this->getRegisteredModelClasses() as $modelClass) {
      $key = $this->getModelClassKey($modelClass);
      $this->modelClassMappings[$key] = $modelClass;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function clearDrupalEntityCacheForModelClass(string $modelClass): self
  {
    $this->entityTypeManager
      ->getStorage($modelClass::entityType())
      ->resetCache();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function clearDrupalEntityCachesForAllModels(): self
  {
    foreach ($this->getRegisteredModelClasses() as $modelClass) {
      $this->clearDrupalEntityCacheForModelClass($modelClass);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasModelClassForEntityAndBundle(string $entity, ?string $bundle): bool
  {
    $key = $this->getKeyForEntityAndBundle($entity, $bundle);
    return array_key_exists($key, $this->modelClassMappings);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegisteredModelClasses(): array
  {
    $modelClasses = [];
    //$modelClasses[] = '\Drupal\spectrum\Models\File';
    $modelClasses[] = Image::class;
    $modelClasses[] = QueuedJob::class;
    $modelClasses[] = RegisteredJob::class;

    return $modelClasses;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleKey(string $modelClass): string
  {
    /** @var ModelInterface $modelClass */
    return empty($modelClass::bundle()) ? $modelClass::entityType() : $modelClass::bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getModelClassKey(string $modelClass): string
  {
    /** @var ModelInterface $modelClass */
    return $this->getKeyForEntityAndBundle($modelClass::entityType(), $modelClass::bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyForEntityAndBundle(string $entity, ?string $bundle): string
  {
    return empty($bundle) ? $entity . '.' . $entity : $entity . '.' . $bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getModelClassForEntityAndBundle(string $entity, ?string $bundle): string
  {
    if ($this->hasModelClassForEntityAndBundle($entity, $bundle)) {
      $key = $this->getKeyForEntityAndBundle($entity, $bundle);
      return $this->modelClassMappings[$key];
    }

    throw new ModelClassNotDefinedException('No model class for entity ' . $entity . ' and bundle ' . $bundle . ' has been defined');
  }

  /**
   * {@inheritdoc}
   */
  public function getModelClassForEntity(EntityInterface $entityInstance): string
  {
    $bundle = $entityInstance->bundle();
    $entity = $entityInstance->getEntityTypeId();

    return $this->getModelClassForEntityAndBundle($entity, $bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function getModelClassForBundleKey(string $bundleKey): ?string
  {
    $foundModelClass = null;
    foreach ($this->getRegisteredModelClasses() as $modelClass) {
      if ($this->getBundleKey($modelClass) === $bundleKey) {
        $foundModelClass = $modelClass;
        break;
      }
    }

    return $foundModelClass;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinitions(string $modelClass): array
  {
    /** @var ModelInterface $modelClass */
    if (empty($modelClass::bundle())) {
      return $this->entityFieldManager->getFieldDefinitions($modelClass::entityType(), $modelClass::entityType());
    }

    return $this->entityFieldManager->getFieldDefinitions($modelClass::entityType(), $modelClass::bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition(string $modelClass, string $fieldName): ?FieldDefinitionInterface
  {
    $fieldDefinition = null;
    $fieldDefinitions = $this->getFieldDefinitions($modelClass);
    if (array_key_exists($fieldName, $fieldDefinitions)) {
      $fieldDefinition = $fieldDefinitions[$fieldName];
    }
    return $fieldDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType(string $modelClass): EntityTypeInterface
  {
    /** @var ModelInterface $modelClass */
    return $this->entityTypeManager->getDefinition($modelClass::entityType());
  }

  /**
   * {@inheritdoc}
   */
  public function getIdField(string $modelClass): string
  {
    return $this->getEntityType($modelClass)->getKeys()['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryFieldByPrettyField(string $modelClass, string $fieldName): string
  {
    $positionOfFirstDot = strpos($fieldName, '.');

    if ($positionOfFirstDot === false) {
      $prettyFieldsToFieldsMap = \Drupal::service('spectrum.model_serializer')->getPrettyFieldsToFieldsMapping($modelClass);

      if (array_key_exists($fieldName, $prettyFieldsToFieldsMap)) {
        return $prettyFieldsToFieldsMap[$fieldName];
      } else {
        throw new \RuntimeException(strtr(
          'Pretty Field @field not found on modelClass @modelClass: ',
          ['@field' => $fieldName, '@modelClass' => $modelClass]
        ));
      }
    } else {
      // Multiple levels found for fieldName, this must be a relationship
      $basePrettyFieldName = substr($fieldName, 0, $positionOfFirstDot);
      $nextPrettyFieldName = substr($fieldName, $positionOfFirstDot + 1);

      /** @var string $modelClass */
      $baseFieldName = $this->getQueryFieldByPrettyField($modelClass, $basePrettyFieldName);

      if ($nextPrettyFieldName === 'id') {
        // In case we request just the id, we can return the field itself, no need to pull in other entities, as Drupal will use the target_id automatically
        return $baseFieldName;
      }

      /** @var FieldRelationship $relationship */
      $relationship = $modelClass::getRelationshipByFieldName($baseFieldName);

      if (empty($relationship)) {
        throw new \RuntimeException(strtr('Relationship not found for field @fieldName', [
          '@fieldName' => $baseFieldName
        ]));
      }

      $fields = [];
      $fields[] = $baseFieldName;
      $fields[] = $this->getQueryFieldByPrettyField($relationship->getFirstModelClass(), $nextPrettyFieldName);

      return implode('.entity.', $fields);
    }
  }
}
