<?php

namespace Drupal\spectrum\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\spectrum\Model\Collection;
use Drupal\spectrum\Model\ModelInterface;
use Drupal\spectrum\Query\Condition;
use Drupal\spectrum\Runnable\Annotation\Job;
use Drupal\spectrum\Runnable\JobInterface;
use Drupal\spectrum\Runnable\QueuedJob;
use Drupal\spectrum\Runnable\RegisteredJob;
use Psr\Log\LoggerInterface;
use Traversable;

/**
 * The Job service is responsible for scheduling, running and registering them
 * in the system.
 */
class JobService extends DefaultPluginManager implements JobServiceInterface {

  /** @var string[] */
  protected array $existingFoundJobs = [];

  /** @var string[] */
  protected array $newRegisteredJobs = [];

  /** @var string[] */
  protected array $removedJobs = [];

  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    private readonly LoggerInterface $logger
  ) {
    parent::__construct('Jobs', $namespaces, $moduleHandler, JobInterface::class, Job::class);

    $this->setCacheBackend($cacheBackend, 'queud_job_plugins');
  }

  public function rebuildRegisteredJobs(): self {
    $definitions = $this->getDefinitions();

    $query = RegisteredJob::getModelQuery();
    /** @var Collection|RegisteredJob[] */
    $currentJobs = $query->fetchCollection();
    $currentJobsByKey = $currentJobs->buildArrayByFieldName('title');

    foreach ($definitions as $key => $definition) {
      if (array_key_exists($key, $currentJobsByKey)) {
        $currentJobsByKey[$key]->selected = TRUE;
        $currentJobsByKey[$key]->setJobClass($definition['class']);
      } else {
        /** @var RegisteredJob $newJob */
        $newJob = $currentJobs->putNew();
        $newJob->setTitle($definition['id']);
        $newJob->setJobClass($definition['class']);
        $newJob->setActive(TRUE);
        $newJob->selected = TRUE;
      }
    }

    foreach ($currentJobs as $job) {
      if ($job->isNew()) {
        $this->newRegisteredJobs[] = $job->getTitle();
      } elseif ($job->selected) {
        $this->existingFoundJobs[] = $job->getTitle();
      } else {
        $this->removedJobs[] = $job->getTitle();
      }
    }

    $currentJobs->removeNonSelectedModels();
    $currentJobs->save();

    return $this;
  }

  public function getAmountOfExistingFoundJobs(): int {
    return sizeof($this->existingFoundJobs);
  }

  public function getAmountOfNewRegisteredJobs(): int {
    return sizeof($this->newRegisteredJobs);
  }

  public function getAmountOfRemovedJobs(): int {
    return sizeof($this->removedJobs);
  }

  /**
   * @return string[]
   */
  public function getExistingFoundJobs(): array {
    return $this->existingFoundJobs;
  }

  /**
   * @return string[]
   */
  public function getNewRegisteredJobs(): array {
    return $this->newRegisteredJobs;
  }

  /**
   * @return string[]
   */
  public function getRemovedJobs(): array {
    return $this->removedJobs;
  }

  public function findExistingJobsForModel(string $jobClass, ModelInterface $model, array $statusses = NULL): Collection {
    $query = QueuedJob::getModelQuery();
    $query
      ->addCondition(new Condition('field_related_entity', '=', $model::entityType()))
      ->addCondition(new Condition('field_related_bundle', '=', $model::bundle()))
      ->addCondition(new Condition('field_related_model_id', '=', $model->getId()));

    if (!empty($statusses)) {
      $query->addCondition(new Condition('field_job_status', 'IN', $statusses));
    }

    $query->addCondition(new Condition('field_job.entity.field_class', '=', $jobClass));

    return $query->fetchCollection();
  }

  public function hasQueuedJobForModel(string $jobClass, ModelInterface $model): bool {
    $collection = $this->findExistingJobsForModel($jobClass, $model, [QueuedJob::STATUS_QUEUED]);
    return $collection !== NULL && sizeof($collection) > 0;
  }

  public function removeQueuedJobsForModel(string $jobClass, ModelInterface $model): self {
    $jobs = $this->findExistingJobsForModel($jobClass, $model, [QueuedJob::STATUS_QUEUED]);
    $jobs->removeAll();
    $jobs->save();

    return $this;
  }

}
