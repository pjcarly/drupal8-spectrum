<?php

declare(strict_types=1);

namespace Drupal\spectrum\Services;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\spectrum\Models\User;

final class CurrentUserService implements CurrentUserServiceInterface {

  private ?User $user = null;

  public function __construct(
    private readonly AccountProxyInterface $currentUser
  ) {
  }

  /**
   * Returns the Current User as a spectrum Model
   *
   * @return User
   */
  public function get(): User {
    if ($this->user === null || $this->user->getId() !== $this->currentUser->id()) {
      $this->user = User::forgeById($this->currentUser->id());
    }

    return $this->user;
  }

  public function getAccountProxy(): AccountProxyInterface {
    return $this->currentUser;
  }

}
