<?php

namespace Drupal\spectrum\Services;

use Drupal\spectrum\Model\Collection;
use Drupal\spectrum\Model\ModelInterface;
use Drupal\spectrum\Runnable\QueuedJob;

/**
 * The Job service is responsible for scheduling, running and registering them in the system.
 */
interface JobServiceInterface
{
  /**
   * Rebuilds all Registered Jobs based on the Jobs that are available in the Code
   *
   * @return self
   */
  public function rebuildRegisteredJobs(): self;

  /**
   * @return integer
   */
  public function getAmountOfExistingFoundJobs(): int;

  /**
   * @return integer
   */
  public function getAmountOfNewRegisteredJobs(): int;

  /**
   * @return integer
   */
  public function getAmountOfRemovedJobs(): int;

  /**
   * @return string[]
   */
  public function getExistingFoundJobs(): array;

  /**
   * @return string[]
   */
  public function getNewRegisteredJobs(): array;

  /**
   * @return string[]
   */
  public function getRemovedJobs(): array;

  /**
   * Finds Jobs in the system that are scheduled for a specific Model (where the Model is the related entity, bundle and id) and optionally a specific status
   *
   * @param string $jobClass
   * @param ModelInterface $model
   * @param string[]|null $status
   * @return Collection<QueuedJob>
   */
  public function findExistingJobsForModel(string $jobClass, ModelInterface $model, array $statusses = null): Collection;

  /**
   * Finds Queued Jobs in the system that are scheduled for a specific Model (where the Model is the related entity, bundle and id)
   *
   * @param string $jobClass
   * @param ModelInterface $model
   * @param string[]|null $status
   * @return Collection<QueuedJob>
   */
  public function hasQueuedJobForModel(string $jobClass, ModelInterface $model): bool;

  /**
   * Removes all Queued Jobs in the system that are scheduled for a specific Model (where the Model is the related entity, bundle and id)
   *
   * @param string $jobClass
   * @param ModelInterface $model
   * @return self
   */
  public function removeQueuedJobsForModel(string $jobClass, ModelInterface $model): self;
}
