<?php

namespace Drupal\spectrum\Serializer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\spectrum\Exceptions\RelationshipNotDefinedException;
use Drupal\spectrum\Model\CollectionInterface;
use Drupal\spectrum\Model\ModelInterface;
use Drupal\spectrum\Services\CurrentUserServiceInterface;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Services\PermissionServiceInterface;
use Drupal\spectrum\Utils\StringUtils;
use Exception;
use Getflights\Jsonapi\Serializer\JsonApiDataNode;
use Getflights\Jsonapi\Serializer\JsonApiNode;
use Getflights\Jsonapi\Serializer\JsonApiRootNode;
use Psr\Log\LoggerInterface;
use RuntimeException;
use stdClass;

final class ModelSerializer implements ModelSerializerInterface {

  /**
   * Here the serialization type aliases will be stored in a shared scope safe
   * way
   *
   * @var array<string, string>
   */
  private array $serializationTypeAliases = [];

  /**
   * Holds an index of the pretty fields to fields mapping, keyed on the
   * modelClass, value an array keyed on the pretty field and value drupal
   * field (for performance this is cached) f.e. ['company' => ['vat-number' =>
   * 'field_vat_number']]
   *
   * @var array<string, array<string, string>>
   */
  protected array $prettyFieldsMapping = [];

  protected array $fieldsMapping = [];

  public function __construct(
    private LoggerInterface $logger,
    private EntityFieldManagerInterface $fieldManager,
    private ModelServiceInterface $modelService,
    private PermissionServiceInterface $permissionService,
    private SerializationControllerInterface $serializationController,
    private CurrentUserServiceInterface $currentUser
  ) {}

  public function getJsonApiNodeForModel(ModelInterface $model): JsonApiNode {
    $serializer = $this->serializationController->getSerializer($model::entityType(), $model::bundle());
    return $serializer->getJsonApiNodeForModel($model);
  }

  public function getJsonApiNodeForCollection(CollectionInterface $collection): JsonApiDataNode {
    $data = new JsonApiDataNode();

    foreach ($collection->getModels() as $model) {
      $node = $this->getJsonApiNodeForModel($model);
      $data->addNode($node);
    }

    return $data;
  }

  public function getSerializedValueForField(ModelInterface $model, string $fieldName): mixed {
    $serializer = $this->serializationController->getSerializer($model::entityType(), $model::bundle());
    $fieldDefinition = $this->modelService->getFieldDefinition($model::class, $fieldName);

    $returnValue = NULL;

    switch ($this->getSerializeAs($fieldDefinition)) {
      case self::SERIALIZE_AS_RELATIONSHIP:
        $returnValue = $serializer->serializeRelationship($model, $fieldName, $fieldDefinition);
        break;
      case self::SERIALIZE_AS_FIELD:
        $returnValue = $serializer->serializeField($model, $fieldName, $fieldDefinition);
        break;
      default:
        throw new Exception('Unknown serialization type');
        break;
    }

    return $returnValue;
  }

  public function getSerializeAs(FieldDefinitionInterface $fieldDefinition): string {
    $returnValue = NULL;

    switch ($fieldDefinition->getType()) {
      case 'entity_reference_revisions':
      case 'entity_reference':
        $fieldObjectSettings = $fieldDefinition->getSettings();

        if (!empty($fieldObjectSettings) && array_key_exists('target_type', $fieldObjectSettings) && $fieldObjectSettings['target_type'] === 'currency') {
          $returnValue = self::SERIALIZE_AS_FIELD;
        } else {
          $returnValue = self::SERIALIZE_AS_RELATIONSHIP;
        }

        break;
      default:
        $returnValue = self::SERIALIZE_AS_FIELD;
        break;
    }

    return $returnValue;
  }

  public function serializeModel(ModelInterface $model): stdClass {
    $root = new JsonApiRootNode();
    $node = $this->getJsonApiNodeForModel($model);
    $root->addNode($node);

    return $root->serialize();
  }

  public function serializeCollection(CollectionInterface $collection): stdClass {
    $root = new JsonApiRootNode();

    $data = $this->getJsonApiNodeForCollection($collection);
    $root->setData($data);

    return $root->serialize();
  }

  public function getDefaultIgnoreFields(): array {
    return [
      'type',
      'revision_log',
      'vid',
      'revision_timestamp',
      'revision_uid',
      'revision_log',
      'revision_translation_affected',
      'revision_translation_affected',
      'revision_default',
      'revision_id',
      'revision_created',
      'path',
      'default_langcode',
      'content_translation_source',
      'content_translation_outdated',
      'content_translation_created',
      'content_translation_changed',
      'content_translation_uid',
      'content_translation_status',
      'preferred_langcode',
      'preferred_admin_langcode',
      'langcode',
      'pass',
      'uuid',
      'metatag',
      'field_meta_tags',
      'menu_link',
      'roles',
      'reusable',
      'rh_action',
      'rh_redirect',
      'rh_redirect_response',
      'behavior_settings',
    ];
  }

  public function deserializeJsonApiIntoModel(ModelInterface $model, stdClass $jsonApiDocument): void {
    $modelClass = $model->getModelClass();
    // get helper variables
    $fieldNameMapping = $this->getPrettyFieldsToFieldsMapping($modelClass);
    $serializer = $this->serializationController->getSerializer($model::entityType(), $model::bundle());

    // and now we'll loop over the different content of the deserialized object
    foreach ($jsonApiDocument->data as $key => $value) {
      switch($key){
        case 'lid':
          $model->setLocalId($value);
          break;
        case 'attributes':
          // here we'll loop all the attributes in the json, and match them to existing attributes on the entity class
          foreach ($value as $attributeKey => $attributeValue) {
            if (array_key_exists($attributeKey, $fieldNameMapping)) {
              $fieldName = $fieldNameMapping[$attributeKey];
              $serializer->deserializeField($model, $fieldName, $attributeValue);
            }
          }
          break;
        case 'relationships':
          foreach ($value as $relationshipFieldName => $relationshipValue) {
            // first we'll check if the relationship exists
            try {
              if (array_key_exists($relationshipFieldName, $fieldNameMapping)) {
                $fieldName = $fieldNameMapping[$relationshipFieldName];
                $serializer->deserializeRelationship($model, $fieldName, $relationshipValue);
              }
            }
            catch (RelationshipNotDefinedException $e) {
              // ignore, the relationship passed doesn't exist
              $this->logger->warning(strtr(
                "@relationship relationship does not exist on @modelClass",
                [
                  "@relationship" => $fieldName,
                  "@modelClass" => $modelClass,
                ]
              ));
            }
          }
          break;
      }
    }
  }

  public function getPrettyFieldForUnderscoredField(string $modelClass, string $underscoredField): string {
    return str_replace('_', '-', $underscoredField);
  }

  /**
   * Checks whether an underscored field exists on this model, this is to
   * correctly translate a field like first_name back to field_first_name
   *
   * @param string $underscoredField
   *
   * @return boolean
   */
  public function underScoredFieldExists(string $modelClass, string $underscoredField): bool {
    $prettyField = $this->getPrettyFieldForUnderscoredField($modelClass, $underscoredField);
    return $this->prettyFieldExists($modelClass, $prettyField);
  }

  public function getPrettyFieldsToFieldsMapping(string $modelClass): array {
    if (!array_key_exists($modelClass, $this->prettyFieldsMapping)) {
      $mapping = [];
      $fieldList = $this->modelService->getFieldDefinitions($modelClass);
      $idField = $this->modelService->getIdField($modelClass);

      foreach ($fieldList as $key => $value) {
        if ($key === $idField) {
          $fieldNamePretty = 'id';
        } else {
          if ($key !== 'title') {
            $fieldNamePretty = trim(trim(StringUtils::dasherize(str_replace('field_', '', $key)), '-'));
          } else {
            $fieldNamePretty = 'name';
          }
        }

        $mapping[$fieldNamePretty] = $key;
      }

      $this->prettyFieldsMapping[$modelClass] = $mapping;
    }

    return $this->prettyFieldsMapping[$modelClass];
  }

  public function getFieldsToPrettyFieldsMapping(string $modelClass): array {
    if (!array_key_exists($modelClass, $this->fieldsMapping)) {
      $prettyMapping = $this->getPrettyFieldsToFieldsMapping($modelClass);
      $mapping = array_flip($prettyMapping);

      $this->fieldsMapping[$modelClass] = $mapping;
    }

    return $this->fieldsMapping[$modelClass];
  }

  public function getFieldForPrettyField(string $modelClass, string $field): ?string {
    $mapping = $this->getPrettyFieldsToFieldsMapping($modelClass);

    return array_key_exists($field, $mapping) ? $mapping[$field] : NULL;
  }

  public function getPrettyFieldForField(string $modelClass, string $field): ?string {
    $mapping = $this->getFieldsToPrettyFieldsMapping($modelClass);

    return array_key_exists($field, $mapping) ? $mapping[$field] : NULL;
  }

  public function prettyFieldExists(string $modelClass, string $field): bool {
    return !empty($this->getFieldForPrettyField($modelClass, $field));
  }

  public function getSerializationType(string $modelClass): string {
    $key = $this->modelService->getModelClassKey($modelClass);
    $alias = array_key_exists($key, $this->serializationTypeAliases) ? $this->serializationTypeAliases[$key] : NULL;

    if (empty($alias)) {
      $returnValue = $this->modelService->getBundleKey($modelClass);
    } else {
      $returnValue = $alias;
    }

    return StringUtils::dasherize($returnValue);
  }

  public function getSerializationModel(string $type): string {
    $types = array_flip($this->serializationTypeAliases);
    $key = $types[$type] ?? throw new RuntimeException('invalid type');

    return $this->modelService->getModelClassForEntityAndBundle(...explode('.', $key));
  }

  /**
   * {@inheritdoc}
   */
  public function setSerializationTypeAlias(string $modelClass, string $type): void {
    $key = $this->modelService->getModelClassKey($modelClass);
    $this->serializationTypeAliases[$key] = $type;
  }

}
