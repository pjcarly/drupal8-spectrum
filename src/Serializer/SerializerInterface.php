<?php

namespace Drupal\spectrum\Serializer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\spectrum\Model\ModelInterface;
use Getflights\Jsonapi\Serializer\JsonApiDataNode;
use Getflights\Jsonapi\Serializer\JsonApiNode;

interface SerializerInterface
{
  /**
   * Deserializes a Relationship into a Drupal entity
   *
   * @param ModelInterface $model
   * @param string $fieldName The drupal fieldname for the entity reference
   * @param mixed $relationshipValue The value of a jsonapidocument related to the attribute
   * @return void
   */
  public function deserializeRelationship(ModelInterface $model, string $fieldName, $relationshipValue): void;

  /**
   * Deserializes the attribute value into a field
   *
   * @param string $fieldName The drupal field name
   * @param mixed $attributeValue The json api attribute value
   * @return void
   */
  public function deserializeField(ModelInterface $model, string $fieldName, $attributeValue): void;

  /**
   * Serializes a Relationshiop into a json api node
   *
   * @param ModelInterface $model
   * @param string $fieldName
   * @param string $fieldNamePretty
   * @param FieldDefinitionInterface|null $fieldDefinition
   * @return JsonApiDataNode|null
   */
  public function serializeRelationship(ModelInterface $model, string $fieldName, ?FieldDefinitionInterface $fieldDefinition): ?JsonApiDataNode;

  /**
   * Serializes a field into any value that will be json_encoded into an attribute on the JsonApi Node
   *
   * @param ModelInterface $model
   * @param string $fieldName
   * @param FieldDefinitionInterface|null $fieldDefinition
   * @return mixed
   */
  public function serializeField(ModelInterface $model, string $fieldName, ?FieldDefinitionInterface $fieldDefinition): mixed;

  /**
   * Serializes a model into a JsonApiNode
   *
   * @param ModelInterface $model
   * @return JsonApiNode
   */
  public function getJsonApiNodeForModel(ModelInterface $model): JsonApiNode;
}
