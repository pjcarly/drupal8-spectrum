<?php

namespace Drupal\spectrum\Serializer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\spectrum\Services\ModelServiceInterface;
use Drupal\spectrum\Services\PermissionServiceInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BaseSerializer implements SerializerInterface, ContainerFactoryPluginInterface, LoggerAwareInterface
{
  public function __construct(
    protected LoggerInterface $logger,
    protected ModelServiceInterface $modelService,
    protected PermissionServiceInterface $permissions,
    protected ModelSerializerInterface $modelSerializer
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(LoggerInterface $logger): void
  {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $container->get("logger.channel.spectrum"),
      $container->get("spectrum.model"),
      $container->get("spectrum.permissions"),
      $container->get("spectrum.model_serializer")
    );
  }
}
