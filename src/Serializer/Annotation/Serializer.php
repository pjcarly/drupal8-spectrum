<?php

namespace Drupal\spectrum\Serializer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an ApiHandler annotation object.
 *
 * @Annotation
 */
class Serializer extends Plugin
{
  public string $id;
  public string $description;
  public string $entity;
  public string $bundle;
  public int $weight = 100;
}
