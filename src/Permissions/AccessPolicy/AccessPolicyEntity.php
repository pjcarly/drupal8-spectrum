<?php

namespace Drupal\spectrum\Permissions\AccessPolicy;

use Drupal\Core\Database\Database;

/**
 * This class is used just to pass an AccessPolicy entry around in the application
 */
class AccessPolicyEntity
{
  /**
   * @var string
   */
  protected $entityType;

  /**
   * @var int
   */
  protected $entityId;

  /**
   * @var string
   */
  protected $id;


  public function __construct(string $entityType, int $entityId, string $id)
  {
    $this->entityType = $entityType;
    $this->entityId = $entityId;
    $this->id = $id;
  }

  /**
   * Sets the Entity Type
   *
   * @param string $entityType
   * @return AccessPolicyEntity
   */
  public function setEntityType(string $entityType): AccessPolicyEntity
  {
    $this->entityType = $entityType;
    return $this;
  }

  /**
   * Returns the entity type
   *
   * @return string
   */
  public function getEntityType(): string
  {
    return $this->entityType;
  }

  /**
   * Sets the Entity ID
   *
   * @param int $entityId
   * @return AccessPolicyEntity
   */
  public function setEntityId(int $entityId): AccessPolicyEntity
  {
    $this->entityId = $entityId;
    return $this;
  }

  /**
   * Returns the entity id
   *
   * @return int
   */
  public function getEntityId(): int
  {
    return $this->entityId;
  }

  /**
   * Sets the User ID
   *
   * @param string $id
   * @return AccessPolicyEntity
   */
  public function setUserId(string $id): AccessPolicyEntity
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Returns the user id
   *
   * @return string
   */
  public function getId(): string
  {
    return $this->id;
  }

  /**
   * Returns an Array of the values of this Entity, that can be used in an InsertQuery
   *
   * @return array
   */
  public function getInsertValue(): array
  {
    return [
      'entity_type' => $this->getEntityType(),
      'entity_id' => $this->getEntityId(),
      'value' => $this->getId(),
    ];
  }
}
