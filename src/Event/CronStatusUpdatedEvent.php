<?php

declare(strict_types=1);

namespace Drupal\spectrum\Event;

use Drupal\spectrum\Runnable\QueuedJob;
use React\EventLoop\LoopInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class CronStatusUpdatedEvent extends Event {

  public function __construct(
    private QueuedJob $queuedJob,
    private ?int $current = NULL,
    private ?int $max = NULL
  ) {
  }

  public function getQueuedJob(): QueuedJob {
    return $this->queuedJob;
  }

  public function getCurrent(): ?int {
    return $this->current;
  }

  public function getMax(): ?int {
    return $this->max;
  }

}
