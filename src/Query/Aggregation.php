<?php

namespace Drupal\spectrum\Query;

/**
 * This class is used to add Aggregations to an Aggregate Query
 */
class Aggregation
{
  /**
   * @param string $fieldName The Drupal field name
   * @param string $aggregateFunction The sorting aggregateFunction
   * @param string|null optional language code
   * @param string|null optional the alias
   */
  public function __construct(
    private string $fieldName,
    private string $aggregateFunction,
    private ?string $langcode = null,
    private ?string $alias = null
  ) {
  }

  /**
   * Get the Drupal field name
   *
   * @return  string
   */
  public function getFieldName(): string
  {
    return $this->fieldName;
  }

  /**
   * @param string $fieldName
   * @return self
   */
  public function setFieldName(string $fieldName): self
  {
    $this->fieldName = $fieldName;
    return $this;
  }

  /**
   * Get the sorting aggregateFunction
   *
   * @return  string
   */
  public function getAggregateFunction(): string
  {
    return $this->aggregateFunction;
  }

  /**
   * @param string $aggregateFunction
   * @return self
   */
  public function setAggregateFunction(string $aggregateFunction): self
  {
    $this->aggregateFunction = $aggregateFunction;
    return $this;
  }

  /**
   * Get the language code
   *
   * @return  string
   */
  public function getLangcode(): ?string
  {
    return $this->langcode;
  }

  /**
   * @param string|null $langcode
   * @return void
   */
  public function setLangCode(?string $langcode)
  {
    $this->langcode = $langcode;
    return $this;
  }

  /**
   * @return string|null
   */
  public function getAlias(): ?string
  {
    return $this->alias;
  }

  /**
   * @param string|null $alias
   * @return void
   */
  public function setAlias(?string $alias)
  {
    $this->alias = $alias;
    return $this;
  }
}
